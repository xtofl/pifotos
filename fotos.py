from pathlib import Path
from favorites import favorites

def fotoindex(structure, elements, isdir, nest=lambda p, c: p+"/"+c):
    for k, v in elements(structure):
        if isdir(v):
            yield from (nest(k, e) for e in fotoindex(v, elements, isdir, nest))
        else:
            yield k

class Fotos:

    def __init__(self, path: Path):
        self.path = path
        self.index = fotoindex(
               path,
               elements=lambda p: zip(p.iterdir(), p.iterdir()),
               isdir=Path.is_dir,
               nest=Path.joinpath)

    def favorites(self, key):
        return favorites(key)
