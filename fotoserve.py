#!/usr/bin/env python3

from flask import Flask, jsonify
from pathlib import Path

app = Flask(__name__)

from fotos import Fotos
fotos = Fotos(Path("www"))

@app.route("/fotos")
def list_fotos():
    return jsonify(tuple(map(str, fotos.index)))

@app.route("/favorites/<who>")
def favorites(who):
    return jsonify(fotos.favorites(who))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
