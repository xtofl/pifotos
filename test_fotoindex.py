from fotos import fotoindex

def test_it_recurses_directories():
    structure = dict(
            file1="afile",
            file2="anotherfile",
            adir=dict(filea="filea"),
            anotherdir=dict(fileb="fileb")
            )
    isdict = lambda e: callable(getattr(e, "items", None))
    assert set(fotoindex(
            structure,
            dict.items,
            isdict,
            nest=lambda a, b: a + "." + b)) == set((
        "file1",
        "file2",
        "adir.filea",
        "anotherdir.fileb"))

